<?php

namespace App\Controller;

use App\Entity\TravelDestination;
use App\Repository\TravelDestinationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboardController extends AbstractController
{
    /**
     * @Route("/aboard", name="aboard")
     */
    public function index(): Response
    {
        return $this->render('aboard/index.html.twig', [
            'controller_name' => 'AboardController',
        ]);
    }
}
