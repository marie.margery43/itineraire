<?php

namespace App\Entity;

use App\Repository\TravelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TravelRepository::class)
 */
class Travel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="travel")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=TravelDestination::class, mappedBy="travels")
     */
    private $traveldestinations;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->traveldestinations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection|TravelDestination[]
     */
    public function getTraveldestinations(): Collection
    {
        return $this->traveldestinations;
    }

    public function addTraveldestination(TravelDestination $traveldestination): self
    {
        if (!$this->traveldestinations->contains($traveldestination)) {
            $this->traveldestinations[] = $traveldestination;
            $traveldestination->setTravels($this);
        }

        return $this;
    }

    public function removeTraveldestination(TravelDestination $traveldestination): self
    {
        if ($this->traveldestinations->removeElement($traveldestination)) {
            // set the owning side to null (unless already changed)
            if ($traveldestination->getTravels() === $this) {
                $traveldestination->setTravels(null);
            }
        }

        return $this;
    }
}
