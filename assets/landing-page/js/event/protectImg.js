const banner = document.querySelector("#banner");

if (banner) {
    banner.addEventListener('contextmenu', (e) => e.preventDefault());
    banner.addEventListener('dragstart', (e) => e.preventDefault());
}