<?php

namespace App\Repository;

use App\Entity\TravelDestination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TravelDestination|null find($id, $lockMode = null, $lockVersion = null)
 * @method TravelDestination|null findOneBy(array $criteria, array $orderBy = null)
 * @method TravelDestination[]    findAll()
 * @method TravelDestination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TravelDestinationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TravelDestination::class);
    }

    // /**
    //  * @return TravelDestination[] Returns an array of TravelDestination objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TravelDestination
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
