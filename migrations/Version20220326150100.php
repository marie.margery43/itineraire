<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220326150100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE travel_user (travel_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_46CB35E4ECAB15B3 (travel_id), INDEX IDX_46CB35E4A76ED395 (user_id), PRIMARY KEY(travel_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE travel_user ADD CONSTRAINT FK_46CB35E4ECAB15B3 FOREIGN KEY (travel_id) REFERENCES travel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE travel_user ADD CONSTRAINT FK_46CB35E4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE travel_destination ADD travels_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE travel_destination ADD CONSTRAINT FK_21450825B5F5F406 FOREIGN KEY (travels_id) REFERENCES travel (id)');
        $this->addSql('CREATE INDEX IDX_21450825B5F5F406 ON travel_destination (travels_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE travel_user');
        $this->addSql('ALTER TABLE travel_destination DROP FOREIGN KEY FK_21450825B5F5F406');
        $this->addSql('DROP INDEX IDX_21450825B5F5F406 ON travel_destination');
        $this->addSql('ALTER TABLE travel_destination DROP travels_id');
    }
}
